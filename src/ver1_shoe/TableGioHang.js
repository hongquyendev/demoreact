import React, { Component } from "react";

export default class TableGioHang extends Component {
  renderTable = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => {
                this.props.tangSoLuong(item.id, -1);
              }}
              className="btn btn-primary mx-2"
            >
              -
            </button>
            {item.soLuong}
            <button
              onClick={() => {
                this.props.tangSoLuong(item.id, 1);
              }}
              className="btn btn-success mx-2"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.xoaItem(item.id);
              }}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTable()}</tbody>

          {/* <tbody>{this.renderTable()}</tbody> */}
          {/* <tbody>{this.renderTable()}</tbody> */}
        </table>
      </div>
    );
  }
}
