import React, { Component } from "react";
import { shoeArr } from "./data_shoeShop";
import ShoeItem from "./ShoeItem";
import TableGioHang from "./TableGioHang";

export default class ShoeVer1 extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [{ ...shoeArr[0], soLuong: 1 }],
  };

  renderShoe = () => {
    return this.state.shoeArr.map((item) => {
      return <ShoeItem shoeData={item} themGioHang={this.themGioHang} />;
    });
  };

  themGioHang = (sp) => {
    let cloneGioHang = [...this.state.gioHang];
    let newSp = { ...sp, soLuong: 1 };
    let viTri = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    if (viTri == -1) {
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[viTri].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };

  tangSoLuong = (idShoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;
    if (cloneGioHang[index].soLuong == 0) {
      cloneGioHang.splice(index, 1);
    }
    this.setState({ gioHang: cloneGioHang });
  };

  xoaItem = (id) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index !== -1) {
      cloneGioHang.splice(index, 1);
    }
    this.setState({
      gioHang: cloneGioHang,
    });
  };

  render() {
    return (
      <div className="container py-5">
        <TableGioHang
          gioHang={this.state.gioHang}
          tangSoLuong={this.tangSoLuong}
          xoaItem={this.xoaItem}
        />
        <div className="row">{this.renderShoe()}</div>
      </div>
    );
  }
}
