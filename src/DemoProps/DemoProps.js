import React, { Component } from "react";
import SanPhamChiTiet from "./SanPhamChiTiet";

export default class DemoProps extends Component {
  state = {
    chiTietSanPham: {
      name: "Iphone 14",
      price: "20",
    },
  };
  render() {
    return (
      <div>
        <SanPhamChiTiet username={"Alice"} detail={this.state.chiTietSanPham} />
      </div>
    );
  }
}
