import React, { Component } from "react";
import { shoeArr } from "./data_shoeShop";
import ItemShoe from "./ItemShoe";
import TableGioHang from "./TableGioHang";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };
  renderShoes = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ItemShoe
          handleAddToCart={this.handleAddToCart}
          shoeData={item}
          key={item.id}
        />
      );
    });
  };

  handleAddToCart = (sp) => {
    // let cloneGioHang = [...this.state.gioHang, shoe];
    // this.setState({ gioHang: cloneGioHang });

    let timViTri = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });

    let cloneGioHang = [...this.state.gioHang];
    if (timViTri == -1) {
      let newSp = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[timViTri].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };

  handleRemove = (idShoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== 1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };

  handleChangeQuantity = (idShoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;
    if (cloneGioHang[index].soLuong == 0) {
      cloneGioHang.splice(index, 1);
    }
    this.setState({ gioHang: cloneGioHang });
  };

  render() {
    return (
      <div className="container py-5">
        {this.state.gioHang.length > 0 && (
          <TableGioHang
            handleRemove={this.handleRemove}
            gioHang={this.state.gioHang}
            handleChangeQuantity={this.handleChangeQuantity}
          />
        )}
        <div className="row">{this.renderShoes()}</div>;
      </div>
    );
  }
}
