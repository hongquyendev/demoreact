import React, { Component } from "react";

export default class DemoStateNumber extends Component {
  state = {
    number: 10,
  };
  handlePlusNumber = () => {
    this.setState({
      number: this.state.number + 1,
    });
  };
  render() {
    return (
      <div>
        <span className="display-4 mx-2">{this.state.number}</span>
        <button className="btn btn-success" onClick={this.handlePlusNumber}>
          Plus number
        </button>
      </div>
    );
  }
}
