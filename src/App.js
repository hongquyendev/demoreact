import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import DemoStyte from "./DemoStyle/DemoStyte";
import DataBinding from "./DataBinding/DataBinding";
import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";
import EventBinding from "./EventBinding/EventBinding";
import Ex_BaiTapChoXe from "./Ex_BaiTapChonXe/Ex_BaiTapChonXe";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import Ex_Phone from "./Ex_Phone/Ex_Phone";
import ShoeVer1 from "./ver1_shoe/ShoeVer1";
import DemoMiniRedux from "./DemoReduxMini/DemoMiniRedux";

function App() {
  return (
    <div className="App">
      {/* <DemoClass></DemoClass> */}
      {/* <DemoClass /> */}
      {/* <DemoFunction /> */}
      {/* <DemoStyte></DemoStyte> */}
      {/* <div className="demo_title">Hello</div> */}
      {/* <DataBinding /> */}
      {/* <ConditionalRendering /> */}
      {/* <EventBinding /> */}
      {/* <Ex_BaiTapChoXe /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <Ex_Phone /> */}
      {/* <ShoeVer1 /> */}
      <DemoMiniRedux />
    </div>
  );
}

export default App;
