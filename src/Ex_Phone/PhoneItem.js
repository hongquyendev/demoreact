import React, { Component } from "react";

export default class PhoneItem extends Component {
  render() {
    let { tenSP, hinhAnh } = this.props.data;
    return (
      <div className="col-4">
        <div className="card" style={{ width: "18rem" }}>
          <img className="card-img-top" src={hinhAnh} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{tenSP}</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <button
              onClick={() => {
                this.props.handleShowDetails(this.props.data);
              }}
              type="button"
              class="btn btn-primary"
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
