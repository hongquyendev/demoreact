import React, { Component } from "react";
import { phoneArr } from "./data_phone";
import PhoneDetails from "./PhoneDetails";
import PhoneList from "./PhoneList";

export default class Ex_Phone extends Component {
  state = {
    phoneArr: phoneArr,
    PhoneDetails: phoneArr[0],
  };

  handleShowDetails = (sp) => {
    this.setState({ PhoneDetails: sp });
  };

  render() {
    return (
      <div className="container py-5">
        <PhoneList
          handleShowDetails={this.handleShowDetails}
          data={this.state.phoneArr}
        />
        <PhoneDetails PhoneDetails={this.state.PhoneDetails} />
      </div>
    );
  }
}
