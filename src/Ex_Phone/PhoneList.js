import React, { Component } from "react";
import shortid from "shortid";
import PhoneItem from "./PhoneItem";

export default class PhoneList extends Component {
  renderPhoneList = () => {
    return this.props.data.map((item, index) => {
      let randomNum = shortid.generate();

      return (
        <PhoneItem
          handleShowDetails={this.props.handleShowDetails}
          data={item}
          key={randomNum}
        />
      );
    });
  };

  render() {
    return <div className="row">{this.renderPhoneList()}</div>;
  }
}
