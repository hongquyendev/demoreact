import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    isLogin: true,
    username: "Alice",
  };
  handleLogin = () => {
    this.setState({
      isLogin: true,
    });
  };
  handleLogout = () => {
    this.setState({
      isLogin: false,
    });
  };
  render() {
    return (
      <div>
        {this.state.isLogin ? (
          <div>
            <p>Đã đăng nhập</p>
            <button className="btn btn-secondary" onClick={this.handleLogout}>
              Logout
            </button>
          </div>
        ) : (
          <div>
            <p>Chưa đăng nhập</p>
            <button className="btn btn-success" onClick={this.handleLogin}>
              Login
            </button>
          </div>
        )}
      </div>
    );
  }
}
