import React, { Component } from "react";

export default class DataBinding extends Component {
  render() {
    let tenSanPham = "Iphone 13";
    let linkSanPham =
      "https://images.fpt.shop/unsafe/filters:quality(90)/fptshop.com.vn/uploads/images/tin-tuc/136895/Originals/Iphone-13-co-may-mau-tong-hop-mau-sac-ro-ri-hap-dan-cua-iphone-13-7.jpg";
    let renderGiaSanPham = () => {
      return "15.000";
    };

    return (
      <div>
        <div className="card" style={{ width: "18rem" }}>
          <img src={linkSanPham} className="card-img-top" alt="..." />
          <div className="card-body">
            {/* inline style: truyền vào Object */}
            <h5
              style={{
                color: "red",
                backgroundColor: "black",
              }}
              className="card-title"
            >
              {tenSanPham}
            </h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              {renderGiaSanPham()}
            </a>
          </div>
        </div>
      </div>
    );
  }
}
