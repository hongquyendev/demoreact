import React, { Component } from "react";
import { connect } from "react-redux";
import { TANG_SO_LUONG } from "./redux/constant/numberConstant";
class DemoMiniRedux extends Component {
  render() {
    return (
      <div>
        <button
          onClick={() => {
            this.props.tangSoLuong(5);
          }}
          className="btn btn-success"
        >
          plus
        </button>
        <span className="text-success display-4">{this.props.number}</span>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.number.giaTri,
  };
  /**
   * key: tên props mà component hiện tại sử dụng
   * value: giá trị trên store
   */
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: (value) => {
      let action = {
        type: TANG_SO_LUONG,
        payload: value,
      };
      // action: mô tả thông cho reducer xử lý
      // dispatch: callback redux sẽ gửi về, nhận action làm tham số
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoMiniRedux);
