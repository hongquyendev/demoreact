import React, { Component } from "react";

export default class EventBinding extends Component {
  isLogin = true;
  handleLogin = () => {
    console.log("yes");
    this.isLogin = false;

    console.log("this.isLogin", this.isLogin);
  };
  handleLoginWithParams = (username) => {
    console.log("Bye" + username);
  };
  renderContent = () => {
    if (this.isLogin) {
      return (
        <>
          <button onClick={this.handleLogin} className="btn btn-success">
            Logout
          </button>
          <butto
            onClick={() => {
              this.handleLoginWithParams(" Bob");
            }}
            className="btn btn-success"
          >
            Logout with username
          </butto>
        </>
      );
    } else {
      return <button className="btn btn-warning">Login</button>;
    }
  };
  render() {
    return (
      <div className="container py-5">
        <div>{this.renderContent()}</div>

        <div> {this.isLogin ? "Đã đăng nhập" : "Chưa đăng nhập"} </div>
      </div>
    );
  }
}
