import React, { Component } from "react";
import { movieArr } from "./data_renderWithMap";
import ItemMovie from "./ItemMovie";

export default class RenderWithMap extends Component {
  state = {
    listMovie: movieArr,
  };

  renderListMovie = () => {
    return this.state.listMovie.map((item, index) => {
      return <ItemMovie movie={item} key={item.maPhim} />;
    });
    // return listItem;
  };
  render() {
    return (
      <div className=" container py-5">
        <div className="row">{this.renderListMovie()}</div>
      </div>
    );
  }
}
